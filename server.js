const express = require ('express');
const app=express();
var bodyParser =require('body-parser');
app.use(bodyParser.json());

function calculateDistance(duration){
        return duration /(2*29);
}
function isDistanceGreaterThanTen(distance){
        if (distance>10)
                return true;
        return false;
}

app.post('/calculateDistance', function (req, res) {
    let duration = req.body.duration;
    let distance = calculateDistance(duration);
    let statusLED= isDistanceGreaterThanTen(distance);
    res.send(statusLED);
  })
  
app.listen(7000)